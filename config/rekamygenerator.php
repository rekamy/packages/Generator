<?php
// TODO: fix format

return [

    /*
    |--------------------------------------------------------------------------
    | App Name
    |--------------------------------------------------------------------------
    |
    | Setup your application name here(For Swagger Use).
    |
    */

    'app_name' => env('APP_NAME', 'Swagger API Documentation'),

    'package_manager' => 'npm',

    'generators' => [
        'frontend' => [
            \Rekamy\Generator\Core\Generators\Frontend\CrudCreateVueGenerator::class,
            \Rekamy\Generator\Core\Generators\Frontend\CrudEditVueGenerator::class,
            \Rekamy\Generator\Core\Generators\Frontend\CrudFormComponentVueGenerator::class,
            \Rekamy\Generator\Core\Generators\Frontend\CrudManageVueGenerator::class,
            \Rekamy\Generator\Core\Generators\Frontend\CrudViewVueGenerator::class,
            \Rekamy\Generator\Core\Generators\Frontend\DatatableGenerator::class,
            
            // \Rekamy\Generator\Core\Generators\Frontend\FrontendModuleGenerator::class,
            // \Rekamy\Generator\Core\Generators\Frontend\YupGenerator::class,
        ],
        'backend' => [
            \Rekamy\Generator\Core\Generators\Backend\APIDocInfoGenerator::class,
            \Rekamy\Generator\Core\Generators\Backend\APIDocGenerator::class,
            \Rekamy\Generator\Core\Generators\Backend\APIRoutesGenerator::class,
            \Rekamy\Generator\Core\Generators\Backend\ModelGenerator::class,
            \Rekamy\Generator\Core\Generators\Backend\RepositoryGenerator::class,
            \Rekamy\Generator\Core\Generators\Backend\BlocGenerator::class,
            \Rekamy\Generator\Core\Generators\Backend\RequestGenerator::class,
            \Rekamy\Generator\Core\Generators\Backend\ControllerGenerator::class,
            \Rekamy\Generator\Core\Generators\Backend\DataPolicyGenerator::class,
        ],
        'mobile' => [],
        'workflow' => [

            // Vue
            \Rekamy\Generator\Core\Generators\Workflow\ViewGenerator::class,
            \Rekamy\Generator\Core\Generators\Workflow\RouteGenerator::class,
            \Rekamy\Generator\Core\Generators\Workflow\ModelGenerator::class,
            \Rekamy\Generator\Core\Generators\Workflow\StoreGenerator::class,

            // Laravel
            \Rekamy\Generator\Core\Generators\Workflow\ControllerGenerator::class,
            \Rekamy\Generator\Core\Generators\Workflow\BlocGenerator::class,
            \Rekamy\Generator\Core\Generators\Workflow\RequestGenerator::class,
            \Rekamy\Generator\Core\Generators\Workflow\EventGenerator::class,

            // TODO:
            // \Rekamy\Generator\Core\Generators\Workflow\ComponentGenerator::class, // vue
        ],
    ],


    /*
    |--------------------------------------------------------------------------
    | Generate
    |--------------------------------------------------------------------------
    |
    | Which file would you like to generate. Set the value to false you
    | don't want to generate.
    |
    | Backend generator
    |   - model
    |   - bloc
    |   - repository
    |   - controller
    |   - request
    |   - exception_validation
    |   - apiDoc
    |   - apiDocInfo
    |   - datatable_criteria
    |   - request_extension_criteria
    |   - length_aware_paginator_overrides
    |   - base_repository_overrides
    |   - request_criteria_overrides
    |   - file_upload
    |   - app_service
    |   - crudable_trait
    |   - crudable_repository_trait
    |   - has_repository_trait
    |   - has_request_trait
    |   - has_auditor_relation_trait
    |   - auth_controller
    |   - base_controller
    |   - crud_bloc
    |   - crud_controller
    |   - crud_repository_interface
    |   - crud_bloc_Interface
    |   - request_interface
    |   - routes_api
    |
    | Frontend generator
    |   - base
    |   - route
    |   - crudIndexVue
    |   - crudIndexTS
    |   - crudCreateVue
    |   - crudCreateTS
    |   - crudViewVue
    |   - crudViewTS
    |   - crudEditVue
    |   - crudEditTS
    |   - frontendModule
    |
    */

    'generate' => [

        // 'backend' => [
        //     'model' => [
        //         'skip' => false,
        //         'class' => null,
        //     ],
        // ],

        // 'frontend' => [
        //     'base' => [
        //         'skip' => false,
        //         'class' => null,
        //     ],

        // ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Database
    |--------------------------------------------------------------------------
    |
    | Database configuration. Set your database name here or
    | from .env and exclude any tables you don't want to generate
    |
    */

    'database' => [

        // Database name
        'name'           => env('DB_DATABASE'),

        // TODO: implements to each generator constructor method
        'only_tables' => [
            // 'attachments'
        ],

        // Exclude table name
        'exclude_tables' => [
            // laravel
            // 'migrations',
            // 'failed_jobs',
            // 'password_resets',

            // // telescope
            // 'telescope_entries_tags',
            // 'telescope_entries',
            // 'telescope_monitoring',

            // // spatie permission
            // 'model_has_permissions',
            // 'model_has_roles',
            // 'role_has_permissions',

            // // passport
            // 'oauth_access_tokens',
            // 'oauth_auth_codes',
            // 'oauth_clients',
            // 'oauth_personal_access_clients',
            // 'oauth_refresh_tokens',
        ],
        'include_tables' => [],
        'skipColumns' => [
            'id',
            'created_at',
            'updated_at',
            'updated_by',
            'created_by',
            'deleted_at', 'deleted_by',
            'remark',
            'password',
            'remember_token',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Paths
    |--------------------------------------------------------------------------
    |
    */

    // Path is where you want the generator to generate.
    'setup' => [
        'backend' => [
            'model' => [
                'path' => base_path('Modules/Crud/Models/'),
                'namespace' => 'Modules\\Crud\\Models',
            ],
            'bloc' => [
                'path' => base_path('Modules/Crud/Bloc/'),
                'namespace' => 'Modules\\Crud\\Bloc',
            ],
            'repository' => [
                'path' => base_path('Modules/Crud/Repositories/'),
                'namespace' => 'Modules\\Crud\\Repositories',
            ],
            'controller' => [
                'path' => base_path('Modules/Crud/Http/Controllers/'),
                'namespace' => 'Modules\\Crud\\Http\\Controllers',
            ],
            'request' => [
                'path' => base_path('Modules/Crud/Http/Requests/'),
                'namespace' => 'Modules\\Crud\\Http\\Requests',
            ],
            'api_doc' => [
                'path' => app_path('APIDoc/'),
                'namespace' => 'App\\APIDoc',
            ],
            'crud_routes' => [
                'path' => base_path('Modules/Crud/Routes/crud.php'),
                'namespace' => null,
            ],
            'api_routes' => [
                'path' => base_path('Modules/Crud/Routes/api.php'),
                'namespace' => null,
            ],
            'policy' => [
                'path' => base_path('App/Traits/'),
                'namespace' => 'App\\Traits',
                'enable' => false,
            ],
        ],
        'frontend' => [
            'build' => 'app',
            'path' => [
                'root' => env('VITE_APP_DIR', 'resources/app/'),
                'module' => 'src/modules/',
                'crud' => 'src/modules/crud/',
            ],
            'source' => 'git@gitlab.com:rekamy/kopenas/frontend.git',
        ],
        'migration' => base_path('database/migrations/'),

    ],

    /*
    |--------------------------------------------------------------------------
    | Namespaces
    |--------------------------------------------------------------------------
    |
    */

    // Namespace for the generated files.
    // 'namespace' => [
    //     'model' => 'App\Models',
    //     'bloc' => 'App\Bloc',
    //     'repository' => 'App\Repositories',
    //     'controller' => 'App\Http\Controllers',
    //     'request' => 'App\Http\Requests',
    //     'exception' => 'App\Exceptions',
    //     'criteria' => 'App\Contracts\Criteria',
    //     'overrides' => 'App\Contracts\Overrides',
    //     'utilities' => 'App\Contracts\Utilities',
    //     'service_provider' => 'App\Providers',
    //     'crudable_trait' => 'App\Contracts\Bloc\Concerns',
    //     'crudable_repository_trait' => 'App\Contracts\Repository\Concerns',
    //     'has_repository_trait' => 'App\Contracts\Bloc\Concerns',
    //     'has_request_trait' => 'App\Contracts\Bloc\Concerns',
    //     'has_auditor_relation_trait' => 'App\Contracts\Bloc\Concerns',
    //     'base_controller' => 'App\Http\Controllers\Base',
    //     'crud_controller' => 'App\Http\Controllers\Base',
    //     'base_bloc' => 'App\Bloc\Base',
    //     'crud_bloc' => '',
    //     'crud_repository_interface' => '',
    //     'crud_bloc_Interface' => '',
    //     'request_interface' => '',
    //     'routes_api' => '',

    //     // 'model'                 => 'App\Models',

    //     // 'repository'            => 'App\Repositories',

    //     // 'bloc'                  => 'App\Bloc',

    //     // 'base_controller'       => 'App\Http\Controllers\Base',

    //     // 'app_base_controller'   => 'App\Http\Controllers',

    //     // 'api_request'           => 'App\Http\Requests\API',

    //     // 'api_controller'        => 'App\Http\Controllers\API',

    //     // 'web_request'           => 'App\Http\Requests',

    //     // 'web_controller'        => 'App\Http\Controllers',
    // ],

    /*
    |--------------------------------------------------------------------------
    | Options
    |--------------------------------------------------------------------------
    |
    */

    // options is an add on you can disable these options by setting the value to false
    'options' => [

        // 'frontend_path' => resource_path('frontend/'),

        // 'backend_path' => base_path(),

        'softDelete' => false,

        'relation' => true,

        'overwrite' => true,

        // if overwrite if true, control overwriting scope
        'dontOverwrite' => [
            app_path('Models/User.php'),
        ],

    ],
    // modules is for specifying which table included if prefix not present
    'modules' => [
        'MODULENAME' => [
            // List of tables
        ]
    ]

];
