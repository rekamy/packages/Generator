<?=
"<?php

namespace {$context->getNamespace('repository')};

use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use {$context->getNamespace('model')}\\$modelName;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class $className
 * @package {$context->getNamespace('repository')}
 *
 * @method $modelName find(\$id, \$columns = ['*'])
 * @method $modelName find(\$id, \$columns = ['*'])
 * @method $modelName first(\$columns = ['*'])
*/
class $className extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected \$fieldSearchable = [\n"?>
<?php
    foreach ($columns as $column) { 
        echo "\t\t'" . $column->getName() . "',\n";
    } 
?>
<?="\t];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return $modelName::class;
    }
    
}

"?>
