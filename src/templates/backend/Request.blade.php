<?=
"<?php

namespace {$context->getNamespace('request')};

use {$context->getNamespace('model')}\\$model;
use {$context->getNamespace('bloc')}\\$blocName;
use Illuminate\Foundation\Http\FormRequest;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Rekamy\LaravelCore\Contracts\CrudableRequest;

class $className extends FormRequest implements CrudableRequest
{
    public function model() {
        return $model::class;
    }

    public function validateIndex()
    {
        /*
        if (!auth()->check()) throw new \Exception(\"Unauthorized Access\", 401);
        \$haveAccess = auth()->user()->can($blocName::permission('index'));
        if (!\$haveAccess) throw new \Exception(\"Unauthorized Processing Request\", 403);
        */
    }

    public function validateStore()
    {
        /*
        if (!auth()->check()) throw new \Exception(\"Unauthorized Access\", 401);
        \$haveAccess = auth()->user()->can($blocName::permission('create'));
        if (!\$haveAccess) throw new \Exception(\"Unauthorized Processing Request\", 403);
        */

        validator(request()->all(), $model::rules('store'))->validate();
    }

    public function validateShow()
    {
        /*
        \$haveAccess = auth()->user()->can($blocName::permission('show'));
        if (!\$haveAccess) throw new \Exception(\"Unauthorized Processing Request\", 403);
        */
    }

    public function validateUpdate()
    {
        /*
        \$haveAccess = auth()->user()->can($blocName::permission('update'));
        if (!\$haveAccess) throw new \Exception(\"Unauthorized Processing Request\", 403);
        */
    }

    public function validateDestroy()
    {
        /*
        \$haveAccess = auth()->user()->can($blocName::permission('destroy'));
        if (!\$haveAccess) throw new \Exception(\"Unauthorized Processing Request\", 403);
        */
    }

    public function rules()
    {
        return [];
        // return $model::rules('default');
    }
}
"?>
