<?=
"<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;
use Modules\Auth\Models\Role;

trait DataPolicy
{

    public static function bootDataPolicy()
    {

        if(!auth()->hasUser()) return;

        \$model = app(get_class());
        \$user = auth()->user();

        \$prefix = \$model->getTable();

        \$permissions = self::getPermissions(\$prefix);
        self::createDataPolicy(\$user, \$model, \$permissions, \$prefix);
        self::readDataPolicy(\$user, \$model, \$permissions, \$prefix);
        self::updateDataPolicy(\$user, \$model, \$permissions, \$prefix);
        self::deleteDataPolicy(\$user, \$model, \$permissions, \$prefix);
    }

    private static function getPermissions(\prefix)
    {
        \user = auth()->user();

        \permissions = \user->getAllPermissions();
        \roles = Role::with([
            'permissions' => function (\q) use (\prefix) {
                \q->where('name', 'like', \prefix.':%');
            },
        ])->whereIn('name', \user->getRoleNames())->get();

        foreach (\roles as \key => \role) {
            foreach (\role->permissions as \key => \permission) {
                \permissions[] = \permission;
            }
        }

        return \permissions;

    }

    private static function filterPermissions($permissions, $name)
    {
        \$model = app(get_class());
        \$prefix = \$model->getTable();

        \$permissions = \$permissions->pluck('name')
            ->filter(function (\$permission) use (\$prefix, \$name) {
                return str(\$permission)->startsWith("\$prefix:\$name");
            });

        // Set priority: If <PERMISSION>:view found, disregard other permission
        foreach (\$permissions as \$key => \$permission) {
            if (\$permission == "\$prefix:\$name") {
                return collect(["\$prefix:\$name"]);
            }
        }

        return \$permissions;

    }

    private static function processPermissions(\$permissions)
    {

        return \$permissions
            ->map(function (\$name) {
                \$name = str(\$name);
                if (\$name->substrCount(':') > 1) {
                    return \$name->afterLast(':')->explode(',');
                }

                return [];
            })
            ->flatten()->unique();

    }

    private static function getValue(\$user, \$columnName)
    {
        \$col = strpos(\$columnName, '=') ? explode('=', \$columnName) : \$columnName;
        \$property = is_array(\$col) ? \$col[1] : 'id';

        if (strpos(\$property, '.')) {
            \$relations = explode('.', \$property);
            \$relative = \$user;
            foreach (\$relations as \$key => \$relation) {
                if (\$key < count(\$relations) - 1) {
                    \$relative->load(\$relation);
                }
                \$value = \$relative->{\$relation};
                \$relative = \$relative->{\$relation};
            }
        } else {
            if (empty(\$user->{\$property})) {
                throw new Exception('Property '.\$property.' not exist (DataPolicy)');
            }
            \$value = \$user->{\$property};
        }

        return $value;

    }

    private static function createDataPolicy(\$user, \$model, \$permissions, \$prefix)
    {
        static::creating(function (\$model) use (\$permissions, \$user) {
            \$permissions = self::filterPermissions(\$permissions, 'create');

            if (\$permissions->isEmpty()) {
                abort(403, 'User doesnt have create permission');
            }

            \$permissions = self::processPermissions(\$permissions);
            // If only <PERMISSION>:create exist, allow create record
            if (\$permissions->isEmpty()) {
                return;
            }

            // Query based on column, if not available use ID in table User
            foreach (\$permissions as \$columnName) {

                \$col = strpos(\$columnName, '=') ? explode('=', \$columnName) : \$columnName;
                \$column = is_array(\$col) ? \$col[0] : \$columnName;
                \$value = self::getValue(\$user, \$columnName);
                if (\$value != \$model->{\$column}) {
                    abort(403, 'User doesnt have create permission');
                }
            }
        });

    }

    private static function readDataPolicy(\$user, \$model, \$permissions, \$prefix)
    {

        static::addGlobalScope('data_policy', function (Builder \$builder) use (\$user, \$model, \$permissions) {

            \$permissions = self::filterPermissions(\$permissions, 'view');
            // Ensure result empty when there is no permission
            if (\$permissions->isEmpty()) {
                \$builder->whereRaw('1=0');

                return;
            }

            \$permissions = self::processPermissions(\$permissions);

            // If only <PERMISSION>:view exist, result based on query
            if (\$permissions->isEmpty()) {
                return;
            }

            // Query based on column, if not available use ID in table User
            foreach (\$permissions as \$columnName) {
                \$col = strpos(\$columnName, '=') ? explode('=', \$columnName) : \$columnName;
                \$column = is_array(\$col) ? \$col[0] : \$columnName;
                \$value = self::getValue(\$user, \$columnName);
                \$builder->where(\$model->table.'.'.\$column, \$value);
            }
        });

    }

    private static function updateDataPolicy(\$user, \$model, \$permissions, \$prefix)
    {

        static::updating(function (\$model) use (\$permissions, \$user) {

            \$permissions = self::filterPermissions(\$permissions, 'update');

            if (\$permissions->isEmpty()) {
                abort(403, 'User doesnt have update permission');
            }

            \$permissions = self::processPermissions(\$permissions);
            // If only <PERMISSION>:update exist, allow update record
            if (\$permissions->isEmpty()) {
                return;
            }

            // Query based on column, if not available use ID in table User
            foreach (\$permissions as \$columnName) {

                \$col = strpos(\$columnName, '=') ? explode('=', \$columnName) : \$columnName;
                \$column = is_array(\$col) ? \$col[0] : \$columnName;
                \$value = self::getValue(\$user, \$columnName);
                if (\$value != \$model->{\$column}) {
                    abort(403, 'User doesnt have update permission');
                }
            }
        });

    }

    private static function deleteDataPolicy(\$user, \$model, \$permissions, \$prefix)
    {
        static::deleting(function (\$model) use (\$permissions, \$user) {

            \$permissions = self::filterPermissions(\$permissions, 'delete');

            if (\$permissions->isEmpty()) {
                abort(403, 'User doesnt have delete permission');
            }

            \$permissions = self::processPermissions(\$permissions);
            // If only <PERMISSION>:update exist, allow update record
            if (\$permissions->isEmpty()) {
                return;
            }

            // Query based on column, if not available use ID in table User
            foreach (\$permissions as \$columnName) {

                \$col = strpos(\$columnName, '=') ? explode('=', \$columnName) : \$columnName;
                \$column = is_array(\$col) ? \$col[0] : \$columnName;
                \$value = self::getValue(\$user, \$columnName);
                if (\$value != \$model->{\$column}) {
                    abort(403, 'User doesnt have delete permission');
                }
            }
        });

    }

    public static function canByActionStatic(\$action, \$model, \$isThrow = true)
    {

        \$user = auth()->user();
        \$prefix = \$model->permission ?? \$model->table;

        \$permissions = self::getPermissions(\$prefix);

        \$permissions = self::filterPermissions(\$permissions, \$action);

        if (\$permissions->isEmpty()) {
            if (\$isThrow) {
                abort(403, \"User doesnt have \$action permission\");
            } else {
                return false;
            }
        }

        \$permissions = self::processPermissions(\$permissions);
        // If only <PERMISSION>:<ACTION> exist, allow <ACTION> record
        if (\$permissions->isEmpty()) {
            return true;
        }

        // Query based on column, if not available use ID in table User
        foreach (\$permissions as \$columnName) {

            \$col = strpos(\$columnName, '=') ? explode('=', \$columnName) : \$columnName;
            \$column = is_array(\$col) ? \$col[0] : \$columnName;
            \$value = self::getValue(\$user, \$columnName);
            if (\$value != \$model->{\$column}) {
                if (\$isThrow) {
                    abort(403, \"User doesnt have \$action permission\");
                } else {
                    return false;
                }
            }
        }
    }

    public function canByAction(\$action, \$isThrow = true)
    {
        \$model = \$this;

        return self::canByActionStatic(\$action, \$model, \$isThrow);
    }
}
";
