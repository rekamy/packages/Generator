<?=
"<?php

namespace {$context->getNamespace('bloc')};

use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use {$context->getNamespace('repository')}\\$repoName;
use {$context->getNamespace('request')}\\$requestName;

class $className extends CrudBloc
{
    public function __construct(
        public $repoName \$repo, 
        public $requestName \$request,
    ) {
        \$this->repo = \$repo;
        \$this->request = \$request;
    }

    public static function permission(\$name) {
    //     \$permission = [
    //         'index' => '{$table}_index',
    //         'create' => '{$table}_create',
    //         'show' => '{$table}_show',
    //         'update' => '{$table}_update',
    //         'destroy' => '{$table}_destroy',
    //     ];
        
    //     return \$permission[\$name];
    }
}
"?>
