<?php use Rekamy\Generator\Core\RuleParser; ?>
@php
@endphp

<?php
echo "
<?php

namespace {$context->getNamespace('model')};

use Illuminate\Database\Eloquent\{SoftDeletes};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Illuminate\Support\Str;
use App\Models\BaseModel as Model;\n" ?>
<?php if($hasRepoTrait): ?>
<?= "use App\RepoTraits\{$RepoTrait};" ?>
<?php endif; ?>
<?php if($config('policy.enable')): ?>
<?= "use App\Traits\DataPolicy;" ?>
<?php endif; ?>
<?= "

class $className extends Model
{"  
?>

<?php if($hasRepoTrait): ?>
<?= "\tuse {$RepoTrait};\n" ?>
<?php endif; ?>
<?php 
    if ($softDelete) :
        echo "\t// use SoftDeletes;\n";
    endif;
    if (config('policy.enable')) :
        echo "\t// use DataPolicy;\n";
    endif;
    ?>
<?= "\tpublic \$table = '$table';\n"?>

<?= "\tpublic \$fillable = [\n\t\t'id',\n"?>
<?php 
    foreach ($columns as $i=> $column) :
        echo "\t\t'{$column->getName()}',\n";
    endforeach;
    echo "\t];\n";
?>

<?= "\tpublic \$casts = [\n" ?>
<?php 
    if($isUuid) :
        echo "\t\t'id' => 'string',\n";
    endif;
    foreach ($columns as $i=> $column) :
        echo "\t\t'{$column->getName()}' => '" . RuleParser::parseCast($column->getType()->getName()) . "',\n";
    endforeach;
    echo "\t];\n";
?>

<?= "\tpublic static function rules(\$scenario = 'default') {\n" ?>
<?= "\t\t\$rules['default'] = [\n" ?>
<?php
    foreach ($notNullColumns as $column) :
        echo "\t\t\t'{$column->getName()}' => 'required',\n";
    endforeach; 
    echo "\t\t];\n";
?>
<?= "\t\t\$rules['store'] = [\n" ?>
<?php
    foreach ($notNullColumns as $column) :
        echo "\t\t\t'{$column->getName()}' => 'required',\n";
    endforeach; 
    echo "\t\t];\n";
?>
<?= "\t\t\$rules['update'] = [\n" ?>
<?php
    foreach ($notNullColumns as $column) :
        echo "\t\t\t'{$column->getName()}' => 'required',\n";
    endforeach; 
    echo "\t\t];\n";
?>
<?= "\t\treturn \$rules[\$scenario];\n" ?>
<?= "\t}\n" ?>
<?php
if($relations) {
    collect($relations)->each(function ($relation) { echo $relation . "\n"; });
}
/*
foreach ($db->get('constraints') as $constraint) {
    if ($constraint->CONSTRAINT_NAME == 'PRIMARY') {
        continue;
    }
    if ($constraint->TABLE_NAME == $table) {
        echo "public function " . Str::camel(Str::singular($constraint->REFERENCED_TABLE_NAME)) . "() \n\t{";
        if ($constraint->REFERENCED_COLUMN_NAME == 'id') {
            echo "\n\t\treturn \$this->belongsTo(" . ucfirst(Str::camel(Str::singular($constraint->REFERENCED_TABLE_NAME))) . "::class);\n";
        }
        echo "\t}\n\n\t";
    }
    if ($constraint->REFERENCED_TABLE_NAME == $table) {
        echo "public function " . Str::camel(Str::singular($constraint->TABLE_NAME)) . "() \n\t{";
        if ($constraint->REFERENCED_COLUMN_NAME == 'id') {
            echo "\n\t\treturn \$this->hasMany(" . ucfirst(Str::camel(Str::singular($constraint->TABLE_NAME))) . "::class, '" . $constraint->COLUMN_NAME . "');\n";
        } else {
            echo "\treturn \$this->hasOne(" . ucfirst(Str::camel(Str::singular($table))) . "::class, '" . $constraint->COLUMN_NAME . "');\n";
        }
        echo "\t}";
    } 
} 
*/
?>
{{ "}" }}
