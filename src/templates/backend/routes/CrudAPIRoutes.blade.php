<?= "
<?php

namespace {$context->getNamespace('controller')};

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the \"api\" middleware group. Enjoy building your API!
|
*/

" ?>

<?= "Route::apiResources([" ?>
<?php foreach ($routes as $route) { ?>
<?= "
    '" . $route['routeName']  . "' => ". $route['className'] ."::class," ?>
<?php } ?>
<?= "
]);" ?>
