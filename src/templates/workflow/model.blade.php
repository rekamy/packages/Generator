<?= "
import type { InferType } from \"yup\";

export const {$model}Schema = object({
" ?>
@php
    foreach ($attributes as $key => $column) {
        $rules = str(data_get($validates, $key));
        $column = str(collect($column)->join(' '));
        $definition = "\t{$key}: ";
    
        //  rules
    
        switch (true) {
            case $rules->contains(['string', 'date', 'datetime', 'datePick', 'datePicker']):
                $definition .= 'string()';
                break;
            case $rules->contains(['number', 'numeric', 'integer']):
                $definition .= 'number().integer()';
                break;
            default:
                $definition .= 'string()';
                break;
        }
    
        $definition .= $rules->contains(['required']) ? '.required()' : '.notRequired()';
    
        //  component based rules
    
        if ($column->contains(['label='])) {
            $label = $column->between('label=', ':')->squish();
            $definition .= ".label({$label})";
        } else {
            $key = str($key)->headline();
            $definition .= ".label(\"{$key}\")";
        }
    
        echo $definition .= ",\n";
    }
@endphp
<?= "});

export type {$model} = InferType<typeof {$model}Schema> & DynamicMap;
" ?>
