export default [
@php
    foreach ($routes as $route => $values) {
        $path = str($route);
        $html = $html->append("\t{\r\t");
        $html = $html->append("\tid: \"{$path->camel()}\",\r\t");
        $html = $html->append("\ttype: \"single\",\r\t");
        $html = $html->append("\tname: \"/{$path->kebab()}\",\r\t");
        $html = $html->append("\tclass: \"fa-solid fa-table-cells-large text-primary\"\r");
    
        $html = $html->append("\t},");
        $html = $html->append("\r");
    }
    
    echo $html->toHtmlString();
@endphp
];
