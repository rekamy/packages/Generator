<?= "
import { defineStore } from \"pinia\";
import type { ValidationError } from \"yup\";
import { crudApi } from \"@/core/api\";
import { type {$studly}, {$studly}Schema as schema } from \"../models/{$kebab}.ts\";

export const use{$studly}Store = defineStore(\"{$kebab}\", {
    state: () => ({
      model: {} as {$studly},
      collections: [] as {$studly}[],
      errors: null,
    }),
    getters: {
      hasError: (state) => !schema?.isValidSync(state.model),
      getError(state) {
        return function (attribute: string) {
          try {
            schema?.validateSyncAt(attribute, state.model);
          } catch (error) {
            const errors = error as ValidationError;
            if (errors.path) return errors.message;
          }
          return null;
        };
      },
    },
    actions: {
      async find(id: ID) {
        this.model = await crudApi<{$studly}>(\"{$kebab}\").find(id);
      },
      async submit() {
        const user = await crudApi<{$studly}>(\"{$kebab}\").create(this.model);
        this.\$reset();
        return user;
      },
    },
  }); 
" ?>