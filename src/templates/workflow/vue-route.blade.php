import BasePage from "@/components/Base/BasePage.vue";
import type { RouteRecordRaw } from "vue-router";

export default [
@php
    foreach($routes as $route => $values) {
        $path = str($route);
        $html = $html->append("\t{\r\t");
        $html = $html->append("\tpath: \"{$path->kebab()}\",\r\t");
        $html = $html->append("\tname: \"{$path->kebab()}\",\r\t");
        $html = $html->append("\tcomponent: () => import(\"./pages/${path}Page.vue\")\r");
        
        $html = $html->append("\t},");
        $html = $html->append("\r");
    }

    echo $html->toHtmlString();
@endphp
] as Array<RouteRecordRaw>;
