@include('generator-templates::workflow.partials.php-tag')

namespace {{ $namespace }};

use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use App\Repositories\{{ $name }}Repository;
use App\Http\Requests\{{ $name }}Request;

class {{ $name }}Bloc extends CrudBloc
{
    public function __construct(
        {{ $name }}Repository $repo,
        {{ $name }}Request $request,
        ) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public static function permission($name): string
    {
        $permission = [
            'index'     => '{{ $name->snake() }}_index',
            'create'    => '{{ $name->snake() }}_create',
            'show'      => '{{ $name->snake() }}_show',
            'update'    => '{{ $name->snake() }}_update',
            'destroy'   => '{{ $name->snake() }}_destroy',
        ];

        return $permission[$name];
    }
}
