<?="
<?php

namespace {$namespace};

use Illuminate\Foundation\Http\FormRequest;

/**
 * @todo {$name}Request:
 */
class {$name}Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return "
?>
@php
    if ($permission) {
        echo "request()->user()->can('{$permission}');";
    } else {
        echo "true;";
    }
@endphp
<?="
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
"?>
@php
    if ($validates) {
        foreach ($validates as $key => $value) {
            echo "\t\t\t" . str("'{$key}' => '{$value}',")->newline()->toHtmlString();
        }
    } else {
        echo "\t\t\t//\n";
    }
@endphp
<?="        ];
    }
}
"?>