{{-- @each('generator-templates::workflow.partials.component', $attributes, 'attribute') --}}
<template>
    <div class="row">
@foreach ($attributes as $field => $attribute)
@if (is_array($attribute))
        <div class="col-6">
        {{-- {{ \Rekamy\Generator\Core\RuleParser::drawNewComponent($field, $attribute)}} --}}
        </div>
@else
        <div class="col-6">
            <RekaInputText
                label="{{str($field)->title()}}"
                v-model="store.model.{{$field}}"
                :error="store.getError('{{$field}}')"
                placeholder="{{str($field)->headline()}}"
                :is-view-only="isViewOnly"
            />
        </div>
@endif
@endforeach
    </div>
    <div class="row">

        <RekaBackButton>Kembali</RekaBackButton>
        @each('generator-templates::workflow.partials.action', $actions, 'action')  
    </div>
</template>

<script setup lang="ts">
@includeWhen(true, 'generator-templates::workflow.partials.import', ['core' => ['one', 'two', 'three']])

// {{ $name }} workflow

defineProps(["store", "isViewOnly"]);

const store = useStore();
const router = useRouter();

@switch(true)
@case(str($name)->contains(['Create', 'create']))
// create ref objects
const {{str($name)->camel()}} = useRepoStore<{{str($name)->studly()}}>("{{str($name)->kebab()}}", {{str($name)->studly()}}Schema);
@break
@case(str($name)->contains(['Update', 'update']))
// update ref objects
@break
@case(str($name)->contains(['Delete', 'delete']))
// delete ref objects
@break
@default
// not in array of {{$name}}
@endswitch

@if ($queries)

// repositories

@each('generator-templates::workflow.partials.repo', $queries, 'action')

// selectors

@each('generator-templates::workflow.partials.select', $queries, 'query')
@endif
// button action methods
@if ($actions)

@each('generator-templates::workflow.partials.script', $actions, 'action')
@endif
</script>
