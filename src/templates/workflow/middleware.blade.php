@include('generator-templates::workflow.partials.php-tag')

namespace {{ $namespace }};

use Illuminate\Foundation\Http\Middleware\PreventRequestsDuringMaintenance as Middleware;

class {{ $classname }} extends Middleware
{
    /**
     * The URIs that should be reachable while maintenance mode is enabled.
     *
     * @var array<int, string>
     */
    protected $except = [
        //
    ];
}
