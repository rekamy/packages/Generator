@include('generator-templates::workflow.partials.php-tag')

namespace {{ $namespace }};

use App\Events\{{ $name }}Event;
use App\Http\Requests\{{ $name }}Request;

class {{ $name }}Controller extends Controller
{
    public function __invoke({{ $name }}Request $request) 
    {
        event(new {{ $name }}Event());

        return response()->json([
            'message'   => 'success',
            'data'      => $event,
        ]);
    }
}
