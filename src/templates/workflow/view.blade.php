@php
    $template = $template->append("
<template>
    <div class=\"row\">");
    
    foreach ($attributes as $key => $attribute) {
        $template = $template->append("\r\t\t<div class=\"col-md-6\">\r\t\t\t")->append('<');
    
        if (!is_array($attribute)) {
            $attribute = explode(' ', $attribute);
        }
        foreach ($attribute as $k => $attr) {
            $attr = str($attr);
            $template = $template->append("{$attr}")->append("\r\t\t\t\t");
        }
    
        $key = str($key);
        $template = $template->append("v-model=\"store.model.{$key}\"")->append("\r\t\t\t\t");
        $template = $template->append(":error=\"store.getError('{$key}')\"")->append("\r\t\t\t\t");
        $template = $template->append("placeholder=\"{$key->ucfirst()}\"")->append("\r\t\t\t\t");
        $template = $template->append(":is-view-only=\"isViewOnly\"")->append("\r\t\t\t/>");
        $template = $template->append("\r\t\t</div>");
    }
    
    $template = $template->append("
    </div>
    <div class=\"row\">
        <RekaBackButton>Kembali</RekaBackButton>");
    
    foreach ($actions as $key => $act) {
        $act = str($act);
        $template = $template->append("\r\t\t<RekaButton class=\"\" color=\"primary\" @click=\"{$act}\">{$act->headline()}</RekaButton>");
    }
    
    $template = $template->append("
    </div>
</template>
");
    
    echo $template;
@endphp

<script setup lang="ts">
    @includeWhen(true, 'generator-templates::workflow.partials.import', ['core' => ['one', 'two', 'three']])

    // {{ $name }} workflow

    defineProps(["store", "isViewOnly"]);

    const store = useStore();
    const router = useRouter();

    @switch(true)
        @case(str($name)->contains(['Create', 'create']))
        // create ref objects
        const {{ str($name)->camel() }} = useRepoStore < {{ str($name)->studly() }} > ("{{ str($name)->kebab() }}",
            {{ str($name)->studly() }}Schema);
        @break

        @case(str($name)->contains(['Update', 'update']))
        // update ref objects
        @break

        @case(str($name)->contains(['Delete', 'delete']))
        // delete ref objects
        @break

        @default
        // not in array of {{ $name }}
    @endswitch

    @if ($queries)

        // repositories

        @each('generator-templates::workflow.partials.repo', $queries, 'action')

        // selectors

        @each('generator-templates::workflow.partials.select', $queries, 'query')
    @endif
    // button action methods
    @if ($actions)

        @each('generator-templates::workflow.partials.script', $actions, 'action')
    @endif
</script>
