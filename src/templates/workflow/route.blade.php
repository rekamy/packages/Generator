<?= "
import BasePage from \"@/components/Base/BasePage.vue\";
import type { RouteRecordRaw } from \"vue-router\";

export default [
    {
        path: \"\",
        name: \"{$path->kebab()}\",
        component: () => import(\"./pages/${path}Page.vue\"),
    },
] as Array<RouteRecordRaw>;
" ?>
