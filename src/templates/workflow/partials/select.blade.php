@php
    $query = str($query);
    $ref = $query->camel();
    $func = $query->start('get-')->camel();
@endphp
<?= "
const {$ref} = ref<any>([]);
const {$func} = async () => {
    {$ref}.value = await crudApi('{$query->kebab()}').asSelection({
        id: 'id',
        name: 'name',
    });
};

{$func}();
" ?>
