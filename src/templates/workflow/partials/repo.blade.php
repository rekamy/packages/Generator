@php
    $action = str($action);
    $type = str($action)->studly();
@endphp
@switch(true)
    @case($action->contains(':'))
        @php
            $action = $action->explode(':');
            $endpoint = str($action[1]);
            $type = str($action[0]);
        @endphp
        <?= "const {$type->camel()}Repo = useRepoStore<{$type->studly()}>(\"{$endpoint->kebab()}\", {$type->studly()}Schema);" ?>
        {{ ' ' }}
    @break

    @default
        <?= "const {$action->camel()}Repo = useRepoStore<{$type}>(\"{$action->kebab()}\", {$type}Schema);" ?>
        {{ ' ' }}
@endswitch
