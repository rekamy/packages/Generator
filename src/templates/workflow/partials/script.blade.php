@php
$action = str($action)->camel();
$template = str("");
@endphp
@switch($action)
@case('save')
<?= "
    const {$action} = () => {
    store.{$action}(route.params.id as string).then(() => {
        widget.alertSuccess('Berjaya!', 'Rekod telah dikemaskini.');
        router.go(-1);
    });
};
" ?>
@break

@case('reject')
<?= "
    const {$action} = () => {
    store.{$action}(route.params.id as string).then(() => {
        widget.alertSuccess('Berjaya!', 'Permohonan dibatalkan.');
        router.go(-1);
    });
};
" ?>
@break

@case('send')
<?= "
    const {$action} = () => {
    store.{$action}(route.params.id as string).then(() => {
        widget.alertSuccess('Berjaya!', 'Request sent.');
        router.go(-1);
    });
};
" ?>
@break

@default
<?= "
    const {$action} = () => {
    store.{$action}(route.params.id as string).then(() => {
        widget.alertSuccess('Berjaya!', 'Rekod telah dikemaskini.');
        router.go(-1);
    });
};
" ?>
@break
@endswitch
