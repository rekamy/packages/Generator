import {
@foreach ($queries as $file)
    {{$file}},
@endforeach
} from 'rekamy/components';
import {
    {{ \Arr::join($core, ',
    ') }}
} from 'rekamy/core';
