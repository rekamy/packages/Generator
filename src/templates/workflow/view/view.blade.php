 <RekaInputText
        wrapperClass="col-6"
        label="{{ str($key)->ucfirst() }}"
        v-model="store.model.{{$key}}"
        :error="store.getError('{{$key}}')"
        placeholder="{{ str($key)->ucfirst() }}"
        :is-view-only="isViewOnly"
    />