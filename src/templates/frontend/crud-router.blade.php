
<?=
"
export default [
  {
    path: \"/crud/${slug}\",
    meta: { title: \"${title}\" },
    children: [
      {
        path: \"\",
        meta: { title: \"${title} List\" },
        component: () => import(\"./pages/Manage${model}Page.vue\"),
      },
      {
        path: \"create\",
        meta: { title: \"Create ${title}\" },
        component: () => import(\"./pages/Create${model}Page.vue\"),
      },
      {
        path: \":id\",
        meta: { title: \"View ${title}\" },
        component: () => import(\"./pages/View${model}Page.vue\"),
      },
      {
        path: \":id/edit\",
        meta: { title: \"Edit ${title}\" },
        component: () => import(\"./pages/Edit${model}Page.vue\"),
      },
    ],
  },
];
" ?>
