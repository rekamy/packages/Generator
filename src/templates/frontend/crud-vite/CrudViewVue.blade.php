<?= "
<template>
  <RekaCard title=\"View {$title}\">
    <{$studly}Component is-view-only />
    <div class=\"d-flex justify-content-end\">
      <RekaBackButton color=\"danger\">Back</RekaBackButton>
      <RekaButton @click=\"\$router.replace(`/crud/{$slug}/\${\$route.params.id}/edit`)\">
        Edit
      </RekaButton>
    </div>
  </RekaCard>
</template>

<script lang=\"ts\" setup>
import {$studly}Component from '../components/{$studly}Component.vue'
</script>
" ?>
