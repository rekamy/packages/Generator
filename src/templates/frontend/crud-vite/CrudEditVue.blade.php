<?php $router = '$router.go(-1)' ?>
<?= "
<template>
    <RekaCard title=\"Edit {$title}\">
        <{$studly}Component />
        <div class=\"d-flex justify-content-end\">
            <RekaBackButton />
            <RekaButton type=\"submit mx-2\" :disabled=\"store.hasErrors\" @click=\"submit\">
              Save
            </RekaButton>
        </div>    
    </RekaCard>
</template>

<script lang=\"ts\" setup>
import {$studly}Component from \"../components/{$studly}Component.vue\"; 
import { use{$studly}Store } from \"../blocs/store\";
import { useRouter } from \"vue-router\";
import { widget } from \"@rekamy/vue-core\";

const store = use{$studly}Store();
const router = useRouter();
const route = useRoute();

const submit = async () => {
  await store.save(true);
  widget.toast(\"Rekod telah dikemaskini.\", \"success\");
  router.replace(`/crud/{$slug}/\${route.params.id}`)
};
</script>
" ?>
