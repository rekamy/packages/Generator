<?= "
<template>
  <RekaCard header-title=\"{$studly}\">
    <template #actionButton>
      <RouterLink class=\"btn btn-sm btn-success\" to=\"/crud/{$slug}/create\">
        Add {$studly}
      </RouterLink>
    </template>
    <RekaDataTable
        ref=\"tableRef\"
        :options=\"options\"
    />
  </RekaCard>
</template>

<script setup lang=\"ts\">
import { ref } from \"vue\";
import { use{$studly}Table } from \"../blocs/table\";
const tableRef = ref();
const { options } = use{$studly}Table(tableRef);
</script>
" ?>
