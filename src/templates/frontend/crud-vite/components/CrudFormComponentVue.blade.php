<?= "
<template>
    <div>"
    ?>
<?php
use Rekamy\Generator\Core\RuleParser;
$scripts = collect();
$chunk = array_chunk($columns->toArray(), 2);
?>
<?= "   <div class=\"row\">"?>
<?php foreach ($columns->toArray() as $key => $column) :
        $element = RuleParser::drawComponent($column);
        if(!empty($element['script'])) $scripts->push(str($element['script'])->replace(':any:', 'SelectOptions[]'))
?>
<?="
            <div class=\"col-md-6\">     
                {$element['component']}
            </div>\n"?>
<?php endforeach; ?>
<?="    </div>"?>
<?= "</div>
</template>

<script setup lang=\"ts\">
import type { SelectOptions } from \"@rekamy/vue-core\";
import { use{$studly}Store } from \"../blocs/store\";"?>
<?php
if (!$scripts->isEmpty())
    echo "\n// import type { {$studly} } from \"../blocs/model\";";
?>
<?="
const props = defineProps<{ isViewOnly?: boolean }>();
const store = use{$studly}Store();
const route = useRoute();

if (route.params.id) store.find(route.params.id as string)
else store.resetModel()

{$scripts->join("\n")}
</script>
" ?>
