<?= "
<template>
  <RekaCard title=\"Create $title\">
    <{$studly}Component />
    <div class=\"d-flex justify-content-end\">
      <RekaBackButton />
      <RekaButton :disabled=\"store.hasErrors\" @click=\"submit\">
        Save
      </RekaButton>
    </div>
  </RekaCard>
</template>

<script lang=\"ts\" setup>
import {$studly}Component from \"../components/{$studly}Component.vue\"; 
import { use{$studly}Store } from \"../blocs/store\";

const store = use{$studly}Store();
const router = useRouter();
const route = useRoute();

const submit = async () => {
  await store.save();
  widget.toast(\"Rekod telah dikemaskini.\", \"success\");
  router.replace(`/crud/{$slug}/\${store.model.id}`)
};
</script>
" ?>
