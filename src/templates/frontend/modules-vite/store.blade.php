import { defineStore } from "pinia";
import type { Maybe } from 'yup'
import { storeApi, hasErrors, getError, type ID } from '@rekamy/vue-core'
import { type {{ $model }}, {{ $model }}Schema as schema } from './model'

export const use{{ $model }}Store = defineStore('{{ $slug }}', {
  state: () => ({
    model: schema.getDefault() as Partial<{{ $model }}>,
    collection: [] as {{ $model }}[],
  }),
  getters: {
    hasErrors: (state) => hasErrors(state.model, schema),
    getError: (state) => (field: string) => 
      getError(state.model, schema, field)
  },
  actions: {
    async fetch(force = false) {
      return await storeApi(this, `crud/${this.$id}`).fetch(force)
    },
    async find(id: Maybe<ID>) {
      return await storeApi(this, `crud/${this.$id}`).find(id)
    },
    async destroy(id: Maybe<ID>) {
      return await storeApi(this, `crud/${this.$id}`).destroy(id)
    },
    async save(edit = false) {
      return await storeApi(this, `crud/${this.$id}`).save(edit);
    },
    resetModel() {
      this.model = schema.getDefault()
    }
  }
})
