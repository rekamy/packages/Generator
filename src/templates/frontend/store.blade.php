import { defineStore } from "pinia";
import type { Maybe, AnyObject } from 'yup'
import { storeApi, hasErrors, getError, type ID } from '@rekamy/vue-core'
import { type {{ $model }}, {{ $model }}Schema as schema } from './model'

export const use{{ $model }}Store = defineStore('{{ $slug }}', {
  state: () => ({
    model: schema.getDefault() as Partial<{{ $model }}>,
    collection: [] as {{ $model }}[],
    query: {
      page: 1,
      perPage: 15,
    }
  }),
  getters: {
    hasErrors: (state) => hasErrors(state.model, schema),
    getError: (state) => (field: string) => 
      getError(state.model, schema, field)
  },
  actions: {
    async prevPage() {
      this.query.page--
      return await this.fetch(true)
    },
    async nextPage() {
      this.query.page++
      return await this.fetch(true)
    },
    async fetch(force = false) {
      return await storeApi(this, `crud/${this.$id}`).fetch(force)
    },
    async first(query?: AnyObject) {
      return await storeApi(this, `crud/${this.$id}`).first(query ?? { latest: true })
    },
    async find(id: Maybe<ID>) {
      return await storeApi(this, `crud/${this.$id}`).find(id)
    },
    async destroy(id: Maybe<ID>) {
      return await storeApi(this, `crud/${this.$id}`).destroy(id)
    },
    async save(edit = false) {
      return await storeApi(this, `crud/${this.$id}`).save(edit);
    },
    resetModel() {
      this.model = schema.getDefault()
    }
  }
})
