import type { InferType } from "yup";
@foreach ($relations as $relation)
@php
    $relationSchemaDir = str($relation['targetTable'])->slug()->singular();
@endphp
import { {{ $relation['targetModel'] }}Schema } from '@/modules/crud/{{ $relationSchemaDir }}/blocs/model'
@endforeach

export const {{ $model }}Schema = object({
@foreach ($columns as $key => $attrs)
@if ($key == 'id')
    {{ $key }}: {{ $parse_attrs($key, $attrs) }},
@else
    {{ $key }}: {{ $parse_attrs($key, $attrs) }},
@endif
@endforeach
});

export const {{ $model }}RelationSchema = object({
@foreach ($relations as $relation)
@php
    $attributeName = str($relation['relName'])->snake()
@endphp
@if ($relation['relType'] == 'belongsTo')
    {{ $attributeName }}: lazy(() => {{ $relation['targetModel'] }}Schema),
@else
    {{ $attributeName }}: lazy(() => array().of({{ $relation['targetModel'] }}Schema)),
@endif
@endforeach
});

export type {{ $model }} = InferType<typeof {{ $model }}Schema & typeof {{ $model }}RelationSchema>;
