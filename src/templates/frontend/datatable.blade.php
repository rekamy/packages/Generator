import type { Ref } from "vue";
import type { DtConfig } from '@@rekamy/vue-component'
import type { {{$model}} } from "./model";

export function use{{$model}}Table(tableRef: Ref) {
    const auth = useAuthStore();
    const router = useRouter();
    const endpoint = crudApi("crud/{{$slug}}").getUrl();
    const options: DtConfig = {
        token: auth.token,
        ajax: {
            url: endpoint,
        },
    order: [[1, "asc"]],
    columns: [
    // { data: "_dtRowIndex", title: "#" },
@php
$hasDescriptiveColumn = collect($columns)->keys()->filter(function($name) {
  return !str($name)->contains(['id', '_at', '_by']);
})->count();
$noOfColumnShowed = 0;
@endphp
@foreach ($columns as $key => $attrs)
@if (str($key)->contains(['id', '_at', '_by']) && $hasDescriptiveColumn || $noOfColumnShowed > 5)
        // { data: "{{ $key }}", title: "{{ $headline($key) }}" },
@else
@php
$noOfColumnShowed++
@endphp
        { data: "{{ $key }}", title: "{{ $headline($key) }}" },
@endif
@endforeach
        {
            searchable: false,
            orderable: false,
            data: "id",
            title: "Tindakan",
            render: dtRenderWrapper(
                dtAction({
                    icon: "fas fa-eye",
                    color: "primary",
                    action: "view",
                }),
                dtAction({
                    icon: "fas fa-pencil",
                    color: "warning",
                    action: "edit",
                }),
                dtAction({
                    icon: "fas fa-trash",
                    color: "danger",
                    action: "destroy",
                    urlTo: "crud/{{$slug}}",
                })
            ),
        },
    ],

    createdRow(element, data) {
        dtActionTrigger(element, "view", () => router.push(`/crud/{{$slug}}/${data.id}`));
        dtActionTrigger(element, "edit", () => router.push(`/crud/{{$slug}}/${data.id}/edit`));
        dtActionTrigger(element, "destroy", () => deleteData(data));
    },
  };

  async function deleteData(data: {{$model}}) {
    try {
      const result = await widget.confirm();
      if (!result.isConfirmed) return;

      await crudApi<{{$model}}>("crud/{{$slug}}").destroy(data.id as string);
      widget.toast("Data anda telah dihapuskan.", "success");
      tableRef.value?.reload();
    } catch (err: unknown) {
      handleError(err);
    }
  }

  return {
    options,
    reload: () => tableRef.value?.reload(),
    search: (keyword: string) => tableRef.value?.search(keyword)
  };
}
