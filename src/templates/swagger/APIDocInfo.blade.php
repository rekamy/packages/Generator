<?=
"<?php

namespace {$namespace};

/**
 * @OA\Info(
 *     version=\"$version\",
 *     title=\"$title\",
 *     description=\"$description\",
 * )
 */
class APIDocInfo {
} 
"?>
