<?php

namespace Rekamy\Generator\Core;

use DB;
use Str;
use Symfony\Component\Yaml\Yaml;

class BuildConfig
{

    public $config;
    public $dbname;
    public $onlyTables = [];
    public $excludeTables = [];
    public $generate;
    public $path;
    public $options;
    // public $namespace;
    public $appName;
    public $template;
    public $db = [];
    // public $output = [];
    public $outputDecorator;
    public $progress;
    public $relations;
    public $console;
    private $moduleName = "";
    private $tablePrefix = "";

    public static function instance()
    {
        return new static(get_called_class());
    }

    public function setModule($name)
    {
        $this->moduleName = $name;
    }

    public function setTablePrefix($name)
    {
        $this->tablePrefix = $name;
    }

    public function getModule()
    {
        return $this->moduleName;
    }

    public function getTablePrefix()
    {
        return $this->tablePrefix;
    }

    public function isModule()
    {
        return $this->moduleName != "";
    }

    public function __construct($console)
    {
        $this->console = $console;
    }

    public function loadConfig()
    {
        $this->config = json_decode(json_encode(config('rekamygenerator')));
        $this->relations = collect();

        // FIXME: on migrate, schema manager does not get latest db struct
        $db = \DB::connection();
        $schema = $db->getDoctrineSchemaManager();

        $this->db = $schema;

        $this->tables = collect($this->db->listTableNames());

        // $this->tables = $this->tables->when(!empty(config('rekamygenerator.database.only_tables')), function ($config) {
        //     return $config->filter(fn ($item) => in_array($item, config('rekamygenerator.database.only_tables')));
        // }, function ($config) {
        //     return $config->filter(fn ($item) => !in_array($item, config('rekamygenerator.database.exclude_tables')));
        // });

        $this->cacheRelationship();
    }

    private function cacheRelationship()
    {
        foreach ($this->tables as $table) {
            collect($this->db->listTableForeignKeys($table))
                ->each(function ($fk) use ($table) {
                    $fkTable = $fk->getForeignTableName();
                    $localKey = $fk->getLocalColumns()[0];
                    $fkKey = $fk->getForeignColumns()[0];

                    $record = [
                        'table' => $table,
                        'relType' => 'belongsTo',
                        'relName' => (string) $this->parseName($localKey)->singular(),
                        'targetTable' => $fkTable,
                        'targetModel' => (string) str($fkTable)->singular()->studly(),
                        'referenceColumn' => $localKey,
                        'targetKey' => $fkKey,
                    ];
                    $this->relations->push($record);
                    $inverseRecord = [
                        'table' => $record['targetTable'],
                        'relType' => 'hasMany',
                        'relName' => $fkKey == 'id' ? (string) $this->parseName($table)->plural() : (string) $this->parseName($fkKey)->plural(),
                        'targetTable' => $table,
                        'targetModel' => (string) str($table)->singular()->studly(),
                        'referenceColumn' => $localKey,
                        'targetKey' => $fkKey,
                        // 'registerBy' => $table,
                    ];
                    $this->relations->push($inverseRecord);
                });
        }
    }

    public function getColumns($table, $skip = [])
    {
        // FIXME: debug this line
        $skip = collect($skip)->toArray();
        $columns = collect($this->db->listTableColumns($table))
            ->filter(fn ($item) => !in_array($item->getName(), $this->config->database->skipColumns))
            ->filter(fn ($item) => !in_array($item->getName(), $skip));

        return $columns;
    }

    public function getTables($db = null)
    {
        $all = collect($this->db->listTableNames());
        if ($this->isModule()) {
            if ($this->getTablePrefix() == "") {
                return $all->filter(fn ($item) => in_array($item, $this->config->module->{$this->getModule()}));
            } 

            return $all->filter(fn ($item) => strpos($item, $this->getTablePrefix()) === 0);
        }
        return $all
            ->merge($this->config->database->include_tables)
            ->filter(fn ($item) => !in_array($item, $this->config->database->exclude_tables));
    }

    private function parseName($name)
    {
        $parsedName = $name = \Str::of($name);
        if ($name->endsWith('_id')) $parsedName = $name->remove('_id');

        return $parsedName->camel();
    }

    public function makeRelation($detail)
    {
        $table = $detail['table'];
        $relType = $detail['relType'];
        $relName = $detail['relName'];
        $targetModel = $detail['targetModel'];

        // if (!class_exists($targetModel)) return;

        $referenceColumn = $detail['referenceColumn'];
        $targetKey = $detail['targetKey'];
        $relModel = ucfirst($relType);

        if ($targetKey == 'id') {
            $relationContext = "return \$this->{$relType}({$targetModel}::class, '$referenceColumn');";
        } else {
            $relationContext = "return \$this->{$relType}({$targetModel}::class, '$referenceColumn', '$targetKey');";
        }

        $html = <<<PHP
        \n\t/**
        \t * Get $table $relName
        \t *
        \t * @return \Illuminate\Database\Eloquent\Relations\\$relModel
        \t */
        \tpublic function $relName() : $relModel
        \t{
        \t    $relationContext
        \t}
        PHP;

        return  $html;
    }

    public function getDescriptorColumn($table)
    {
        // FIXME: check implementation
        $fkColumns = collect($this->db->listTableForeignKeys($table))
            ->map(fn ($col) => $col->getColumns()[0])->values();
        // $indexColumns = collect($this->db->listTableIndexes($table))
        //     ->map(fn ($col, $key) => ['column' => $col->getColumns()[0], 'table' => $key]);
        $indexColumns = collect($this->db->listTableIndexes($table))
            ->filter(fn ($col, $key) => $key == 'primary')
            ->map(fn ($col) => $col->getColumns()[0])->values();

        $skipColumns = collect()
            ->merge($fkColumns)
            ->merge($indexColumns)
            ->merge($this->config->database->skipColumns)->unique();

        return $this->getColumns($table, $skipColumns)
            ->first(fn ($col) => !\Str::endsWith($col->getName(), '_id'));
    }

    public function generateFiles($generators)
    {
        foreach ($generators as $class) {
            $generator = new $class($this);
            $generator->generate();
            $this->console->newline();
        }
    }

    public function __call(string $method, array $arguments)
    {
        if (!method_exists($this, $method)) {
            call_user_func([$this->console, $method], ...$arguments);
            // $this->console->$method(...$arguments);
        }
    }

    public function getNamespace($type) {
        if($this->isModule()) {
            return str_replace('Crud', $this->getModule(), data_get(config('rekamygenerator.setup.backend'), "$type.namespace"));
        }
        return data_get(config('rekamygenerator.setup.backend'), "$type.namespace");
    }

    public function getPath($type) {
        if($this->isModule()) {
            return str_replace('Crud', $this->getModule(), data_get(config('rekamygenerator.setup.backend'), "$type.path"));
        }
        return data_get(config('rekamygenerator.setup.backend'), "$type.path");
    }
}
