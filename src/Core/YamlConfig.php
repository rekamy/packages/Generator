<?php

namespace Rekamy\Generator\Core;

use Illuminate\Contracts\View\View;
use Symfony\Component\Yaml\Yaml;
use Illuminate\Support\Facades\DB;

trait YamlConfig
{
    public function testYaml(): void
    {
        dd('tested!');
    }

    public function tableList(): array
    {
        return DB::getDoctrineSchemaManager()->listTables();
    }

    /**
     * The real path to the package files.
     *
     * @param string $path
     *
     * @return string
     */
    public function generateFiles($generators)
    {
        foreach ($generators as $class) {
            $generator = new $class($this);
            $generator->generate();
            $this->newline();
        }
    }

    /**
     * Prepare and render passed props in view() method.
     */
    public function handleTemplate(View $template, string $flow, mixed $props = []): string
    {
        $props = $this->handleProps($flow, $props);
        return $template->with($props->toArray())->render();
    }

    /**
     * The real path to the package files.
     *
     * @param string $path
     *
     * @return string
     */
    public static function path(string $path = ''): string
    {
        $current = dirname(__DIR__, 2);

        return realpath($current . ($path ? DIRECTORY_SEPARATOR . $path : $path));
    }

    public function yaml(): Yaml
    {
        return app(Yaml::class);
    }

    /**
     * Dumps a PHP value to a YAML string.
     *
     * The dump method, when supplied with an array, will do its best
     * to convert the array into friendly YAML.
     *
     * @param mixed $input  The PHP value
     * @param int   $inline The level where you switch to inline YAML
     * @param int   $indent The amount of spaces to use for indentation of nested nodes
     * @param int   $flags  A bit field of DUMP_* constants to customize the dumped YAML string
     */
    public function dump(array $object)
    {
        return $this->yaml()->dump($object, 5, 2, Yaml::DUMP_EMPTY_ARRAY_AS_SEQUENCE);
    }

    /**
     * Parses YAML into a PHP value.
     *
     *  Usage:
     *  <code>
     *   $array = Yaml::parse(file_get_contents('config.yml'));
     *   print_r($array);
     *  </code>
     *
     * @param string $input A string containing YAML
     * @param int    $flags A bit field of PARSE_* constants to customize the YAML parser behavior
     *
     * @throws ParseException If the YAML is not valid
     */
    public function parse(string $literal, int $flags = 0)
    {
        return $this->yaml()->parse($literal, $flags);
    }

    /**
     * Reads any file returning a string.
     *
     * Usage:
     *
     *     $array = Yaml::parseFile('config.yml');
     *     print_r($array);
     *
     * @param string $filename The path to the file to be parsed
     * @param bool    $asArray Returns multiline strings as PHP value
     * @return mixed
     */
    public function readFile(string $filename, bool $asArray = false)
    {
        if ($asArray)
            return file(__DIR__ . $filename);

        return file_get_contents(__DIR__ . $filename);
    }

    /**
     * Reads a YAML file returning PHP values.
     *
     * Usage:
     *
     *     $array = Yaml::parseFile('config.yml');
     *     print_r($array);
     *
     * @param string $filename The path to the YAML file to be parsed
     * @param int    $flags    A bit field of PARSE_* constants to customize the YAML parser behavior
     *
     * @throws ParseException If the file could not be read or the YAML is not valid
     */
    public function readYaml(string $filename, int $flags = 0)
    {
        return $this->yaml()->parseFile($filename, $flags);
    }

    /**
     * Write parsed PHP values into a YAML file.
     *
     * The dump method, when supplied with an array, will do its best
     * to convert the array into friendly YAML.
     *
     * @param mixed $input  The PHP value
     * @param int   $inline The level where you switch to inline YAML
     * @param int   $indent The amount of spaces to use for indentation of nested nodes
     * @param int   $flags  A bit field of DUMP_* constants to customize the dumped YAML string
     */
    public function writeYaml(string $path, mixed $object = [])
    {
        // \File::ensureDirectoryExists();
        \File::put($path, $this->dump($object));
        // file_put_contents($path ?? __DIR__ . '/../schema.yaml', $this->dump($object));

        return $this;
    }
}
