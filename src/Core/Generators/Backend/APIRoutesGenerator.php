<?php

namespace Rekamy\Generator\Core\Generators\Backend;

use DB;
use Rekamy\Generator\Core\RuleParser;
use Rekamy\Generator\Core\StubGenerator;
use Illuminate\Support\Str;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Helper\TableCell;

class APIRoutesGenerator
{
    private $context;
    private $tables;

    public function __construct($context)
    {
        $this->context = $context;
        $this->context->info("Creating APIRoutes...");
        $this->tables = $this->context->getTables();
    }

    public function generate()
    {
        try {
            $this->crudRoutes();
            // $this->authRoutes();
            $this->appendRoutes();
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function authRoutes()
    {
        $view = view('generator-templates::backend.routes.AuthAPIRoutes');

        $stub = new StubGenerator(
            $this->context,
            $view->render(),
            $this->context->config->setup->backend->api_routes->path
        );

        $stub->render();
        $this->context->info("Api Route Created.");
    }

    public function crudRoutes()
    {
        $data = [
            'routes' => [],
        ];
        foreach ($this->tables as $key => $table) {
            $data['routes'][] = [
                "className" => Str::of($table)->singular()->studly() . "Controller",
                "routeName" => Str::of($table)->singular()->slug()
            ];
        }

        $data['context'] = $this->context;

        $view = view('generator-templates::backend.routes.CrudAPIRoutes', $data);

        $stub = new StubGenerator(
            $this->context,
            $view->render(),
            // $this->context->config->setup->backend->api_routes->path

            $this->context->getPath('crud_routes')
            // $this->context->config->setup->backend->crud_routes->path
        );

        $stub->render();
        $this->context->info("Api Route Created.");
    }

    public function appendRoutes()
    {
        $target = config('rekamygenerator.setup.backend.api_routes.path');
        $uncomment = str(file_get_contents($target))->replace('// require_once', 'require_once');
        file_put_contents($target, $uncomment);
    }
}
