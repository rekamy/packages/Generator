<?php

namespace Rekamy\Generator\Core\Generators\Backend;

use DB;
use Rekamy\Generator\Core\RuleParser;
use Rekamy\Generator\Core\StubGenerator;
use Illuminate\Support\Str;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Helper\TableCell;

class DataPolicyGenerator
{
    private $context;

    public function __construct($context)
    {
        $this->context = $context;
        $this->context->info("Creating DataPolicy Traits...");
    }

    public function generate()
    {
        try {
            $view = view('generator-templates::backend.DataPolicy')
                ->with('context', $this->context);

            $stub = new StubGenerator(
                $this->context,
                $view->render(),
                $this->context->config->setup->backend->policy->path . 'DataPolicy.php'
            );

            $stub->render();
            $this->context->info("DataPolicy Traits Created.");
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
