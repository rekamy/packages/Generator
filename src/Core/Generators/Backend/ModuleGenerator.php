<?php

namespace Rekamy\Generator\Core\Generators\Backend;

use Rekamy\Generator\Core\Generators\BaseGenerator;
use Rekamy\Generator\Core\YamlConfig;

class ModuleGenerator extends BaseGenerator
{
  use YamlConfig;

  public function generate()
  {
    $tables = $this->tableList();
    
    if (file_exists(base_path('Modules/Crud'))) {
        $this->context->call('module:delete Crud');
    }
    
    $this->context->call('module:make Crud');
    
    dd('wip');
  }

  public function parse_attrs(mixed $name, mixed $attrs)
  {
    $name = str($name);
    $rule = str('');

    switch (true) {
      case $name->startsWith('is_'):
        $rule = $rule->append('boolean().default(false)');
        break;
      case $name->endsWith('_id'):
        $rule = $rule->append('string()');
        break;
      case $name->contains(['_at', '_by']):
        $rule = $rule->append('string()');
        break;
      default:
        $rule = $rule->append('string()');
        break;
    }

    $rule = $attrs->getNotNull() ? $rule->append('.required()') : $rule->append('.notRequired()');

    $rule = $rule->append(".label(\"{$name->headline()}\")");

    if ($name == 'id') {
        $rule = $rule->replace('required()', 'notRequired()');
    }

    return $rule->toHtmlString();
  }

  public function handleProps(string $flow, mixed $props = [])
  {
    return collect([
      'model'       => str($flow)->studly(),
      'slug'        => str($flow)->slug(),
      'columns'     => $props,
      'parse_attrs' => $this->parse_attrs(...),
      'headline' => fn ($s) => str($s)->headline(),
    ]);
  }
}
