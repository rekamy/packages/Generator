<?php

namespace Rekamy\Generator\Core\Generators;

class BaseGenerator
{
    public function __construct(public $context)
    {
        $name = str_replace(class_basename($this), 'Generator', '');
        $this->context = $context;
        $this->context->info("Creating {$name}...");
    }
}
