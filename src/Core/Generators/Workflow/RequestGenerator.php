<?php

namespace Rekamy\Generator\Core\Generators\Workflow;

use Rekamy\Generator\Core\Generators\BaseGenerator;
use Rekamy\Generator\Core\YamlConfig;

class RequestGenerator extends BaseGenerator
{
    use YamlConfig;

    public function generate()
    {
        foreach ($this->context->workflow as $module => $workflows) {
            $wkdir = "app/Http/Requests/";
            if (!file_exists($wkdir)) mkdir($wkdir, 0644, true);
            $this->context->newline()->comment($wkdir);
            foreach ($workflows as $workflow => $values) {
                $template = $this->handleTemplate(view('generator-templates::workflow.request'), $workflow, $values);
                $file = str($workflow)->append('Request.php')->studly();
                file_put_contents("{$wkdir}{$file}", $template);
                $this->context->newline()->line('    |_ ' . $file);
            }
        }
    }

    public function handleProps(string $flow, mixed $props = [])
    {
        return collect([
            'name'          => str($flow),
            'namespace'     => 'App\Http\Requests',
            'permission'    => data_get($props, 'permission'),
            'attributes'    => data_get($props, 'attributes'),
            'validates'     => data_get($props, 'validates'),
        ]);
    }
}
