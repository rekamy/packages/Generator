<?php

namespace Rekamy\Generator\Core\Generators\Workflow;

use Rekamy\Generator\Core\Generators\BaseGenerator;
use Rekamy\Generator\Core\YamlConfig;

class ComponentGenerator extends BaseGenerator
{
    use YamlConfig;

    public function generate()
    {
        foreach ($this->context->workflow as $workflow => $values) {
            $template = $this->handleTemplate(view('generator-templates::workflow.component'), $workflow, $values);
            $wkdir = "workflow/components";

            if (!file_exists($wkdir)) mkdir($wkdir);

            file_put_contents("{$wkdir}/{$workflow}.vue", $template);
            $this->context->newline()->comment("{$wkdir}/{$workflow}.vue");
        }

    }

    public function handleProps(string $flow, mixed $props = [])
    {
        return collect([
            'name'          => $flow,
            'attributes'    => data_get($props, 'attributes'),
            'actions'       => data_get($props, 'actions'),
            'scripts'       => data_get($props, 'scripts'),
            'queries'       => data_get($props, 'queries'),
        ]);
    }
}
