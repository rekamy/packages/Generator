<?php

namespace Rekamy\Generator\Core\Generators\Workflow;

use Rekamy\Generator\Core\Generators\BaseGenerator;
use Rekamy\Generator\Core\YamlConfig;

class StoreGenerator extends BaseGenerator
{
    use YamlConfig;

    public function generate()
    {
        foreach ($this->context->workflow as $module => $workflows) {
            $wkdir = "{$module}/stores/";
            if (!file_exists($wkdir)) mkdir($wkdir, 0644, true);
            $this->context->newline()->comment($wkdir);
            foreach ($workflows as $workflow => $values) {
                $template = $this->handleTemplate(view('generator-templates::workflow.store'), $workflow, $values);
                $file = str($workflow)->kebab();
                file_put_contents("{$wkdir}{$file}.ts", $template);
                $this->context->newline()->line('    |_ ' . $file . '.ts');
            }
        }
    }

    public function handleProps(string $flow, mixed $props = [])
    {
        return collect([
            'name'      => $flow,
            'store'     => str($flow),
            'studly'    => str($flow)->studly(),
            'camel'     => str($flow)->camel(),
            'kebab'     => str($flow)->kebab(),
        ]);
    }
}
