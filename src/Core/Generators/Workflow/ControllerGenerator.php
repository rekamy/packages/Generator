<?php

namespace Rekamy\Generator\Core\Generators\Workflow;

use Rekamy\Generator\Core\Generators\BaseGenerator;
use Rekamy\Generator\Core\YamlConfig;

class ControllerGenerator extends BaseGenerator
{
    use YamlConfig;

    public function generate()
    {
        foreach ($this->context->workflow as $module => $workflows) {
            $wkdir = "app/Http/Controllers/";
            if (!file_exists($wkdir)) mkdir($wkdir, 0644, true);
            $this->context->newline()->comment($wkdir);
            foreach ($workflows as $workflow => $values) {
                $template = $this->handleTemplate(view('generator-templates::workflow.controller'), $workflow, $values);
                $file = str($workflow)->append('Controller.php')->studly();
                file_put_contents("{$wkdir}{$file}", $template);
                $this->context->newline()->line('    |_ ' . $file);
            }
        }
    }

    public function handleProps(string $flow, mixed $props = [])
    {
        return collect([
            'name'          => str($flow),
            'namespace'     => 'App\Http\Controllers',
            // 'attributes'    => data_get($props, 'attributes'),
            // 'actions'       => data_get($props, 'actions'),
            // 'scripts'       => data_get($props, 'scripts'),
            // 'queries'       => data_get($props, 'queries'),
        ]);
    }
}
