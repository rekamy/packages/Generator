<?php

namespace Rekamy\Generator\Core\Generators\Workflow;

use Rekamy\Generator\Core\Generators\BaseGenerator;
use Rekamy\Generator\Core\YamlConfig;

class ModelGenerator extends BaseGenerator
{
    use YamlConfig;

    public function generate()
    {
        foreach ($this->context->workflow as $module => $workflows) {
            $wkdir = "{$module}/models/";
            if (!file_exists($wkdir)) mkdir($wkdir, 0644, true);
            $this->context->newline()->comment($wkdir);
            foreach ($workflows as $workflow => $values) {
                $template = $this->handleTemplate(view('generator-templates::workflow.model'), $workflow, $values);
                $file = str($workflow)->kebab();
                file_put_contents("{$wkdir}{$file}.ts", $template);
                $this->context->newline()->line('    |_ ' . $file . '.ts');
            }
        }
    }

    public function handleProps(string $flow, mixed $props = [])
    {
        return collect([
            'classname'     => $flow,
            'model'         => str($flow)->studly(),
            'permission'    => data_get($props, 'permission'),
            'attributes'    => data_get($props, 'attributes'),
            'validates'     => data_get($props, 'validates'),
            'parser'        => collect([
                'string'    => 'string()',
                'numeric'   => 'number().integer()',
                'required'  => 'required()',
                'null'      => 'notRequired()',
                'unique'    => 'unique()',
            ]),
        ]);
    }
}
