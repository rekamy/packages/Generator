<?php

namespace Rekamy\Generator\Core\Generators\Workflow;

use Rekamy\Generator\Core\Generators\BaseGenerator;
use Rekamy\Generator\Core\YamlConfig;

class RouteGenerator extends BaseGenerator
{
    use YamlConfig;

    public function generate()
    {
        foreach ($this->context->workflow as $module => $workflows) {
            $wkdir = "{$module}/";
            if (!file_exists($wkdir)) mkdir($wkdir, 0644, true);
            $file = str($wkdir);
            $this->context->newline()->comment($file->beforeLast('/'));

            // router.ts
            $template = $this->handleTemplate(view('generator-templates::workflow.vue-route'), $module, $workflows);
            $router = $file->append('router.ts');
            file_put_contents($router, $template);
            $this->context->newline()->line('    |_ ' . 'router.ts');
            
            // menu.ts
            $template = $this->handleTemplate(view('generator-templates::workflow.vue-menu'), $module, $workflows);
            $menus = $file->append('menu.ts');
            file_put_contents($menus, $template);
            $this->context->newline()->line('    |_ ' . 'menu.ts');
        }

    }

    public function handleProps(string $flow, mixed $props = [])
    {
        return collect([
            'path'      => str($flow),
            'html'      => str(""),
            'routes'    => $props,
        ]);
    }
}
