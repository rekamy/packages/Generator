<?php

namespace Rekamy\Generator\Core\Generators\Workflow;

use Rekamy\Generator\Core\Generators\BaseGenerator;
use Rekamy\Generator\Core\YamlConfig;

class EventGenerator extends BaseGenerator
{
    use YamlConfig;

    public function generate()
    {
        
        foreach ($this->context->workflow as $module => $workflows) {
            $this->context->newline()->comment('Generating Events and Listeners');
            foreach ($workflows as $workflow => $values) {
                // $template = $this->handleTemplate(view('generator-templates::workflow.bloc'), $workflow, $values);
                // $file = str($workflow)->append('Bloc.php')->studly();
                // file_put_contents("{$wkdir}{$file}", $template);
                // $this->context->newline()->line('    |_ ' . $file);
                $this->context->newline()->line('    |_ ' . $workflow . 'Event');
                $this->context->callSilently('make:event', ['name' => $workflow . 'Event']);
                $this->context->callSilently('make:listener', ['name' => $workflow . 'Listener']);
                $this->context->newline()->line('    |_ ' . $workflow . 'Listener');
            }
        }
    }

    public function handleProps(string $flow, mixed $props = [])
    {
        return collect([
            'name'          => str($flow),
            'namespace'     => 'App\Bloc',
            // 'attributes'    => data_get($props, 'attributes'),
            // 'actions'       => data_get($props, 'actions'),
            // 'scripts'       => data_get($props, 'scripts'),
            // 'queries'       => data_get($props, 'queries'),
        ]);
    }
}
