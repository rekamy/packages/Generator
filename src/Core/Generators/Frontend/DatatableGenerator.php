<?php

namespace Rekamy\Generator\Core\Generators\Frontend;

use Illuminate\Support\Facades\DB;
use Rekamy\Generator\Core\Generators\BaseGenerator;
use Rekamy\Generator\Core\YamlConfig;

class DatatableGenerator extends BaseGenerator
{
    use YamlConfig;

    public function generate()
    {
        $tables = DB::getDoctrineSchemaManager()->listTables();

        foreach ($tables as $key => $table) {

            $wkdir = str(config('rekamygenerator.setup.frontend.path.root'));

            $bloc = str($table->getName());
            $blocs = $wkdir->append("src/modules/crud/" . $bloc->singular())
                ->append("/blocs")->replace('_', '-');

            if (!file_exists($blocs)) mkdir($blocs, 0644, true);

            $this->context->console->newline()->comment($blocs);
            $args = [
                $bloc->singular(),
                [
                    'columns' => $table->getColumns(),
                    'relations' => $this->context->relations->where('table', $table->getName())
                ]
            ];

            $dt = $this->handleTemplate(view('generator-templates::frontend.datatable'), ...$args);
            $route = $this->handleTemplate(view('generator-templates::frontend.crud-router'), ...$args);
            $yup = $this->handleTemplate(view('generator-templates::frontend.yup-model'), ...$args);
            $store = $this->handleTemplate(view('generator-templates::frontend.store'), ...$args);
            // $main = $this->handleTemplate(view('generator-templates::frontend.crud-vite.CrudMainVue'), ...$args);

            file_put_contents("{$blocs}/table.ts", $dt);
            $this->context->console->newline()->line('    |_ table.ts');
            file_put_contents("{$blocs}/../router.ts", $route);
            $this->context->console->newline()->line('    |_ router.ts');
            file_put_contents("{$blocs}/model.ts", $yup);
            $this->context->console->newline()->line('    |_ model.ts');
            file_put_contents("{$blocs}/store.ts", $store);
            $this->context->console->newline()->line('    |_ store.ts');
            // file_put_contents("{$blocs}/../pages/MainPage.vue", $main);
            // $this->context->console->newline()->line('    |_ MainPage.vue');

            // $this->context->console->newline()->line('    |_ index.ts');
        }
    }

    public function parse_attrs(mixed $name, mixed $attrs)
    {
        $name = str($name);
        $rule = str('');

        switch (true) {
            case $name->startsWith('is_'):
                $rule = $rule->append('boolean().default(false)');
                break;
            case $name->endsWith('_id'):
                $rule = $rule->append('string()');
                break;
            case $name->contains(['_at', '_by']):
                $rule = $rule->append('string()');
                break;
            default:
                $rule = $rule->append('string()');
                break;
        }

        $rule = $attrs->getNotNull() ? $rule->append('.required()') : $rule->append('.notRequired()');

        $rule = $rule->append(".label(\"{$name->headline()}\")");

        if ($name == 'id') {
            $rule = $rule->replace('required()', 'notRequired()');
        }

        return $rule->toHtmlString();
    }

    public function handleProps(string $flow, mixed $props = [])
    {
        return collect([
            'model'       => str($flow)->studly(),
            'title'       => str($flow)->headline(),
            'slug'        => str($flow)->slug(),
            'columns'     => $props['columns'],
            'relations'   => $props['relations'],
            'parse_attrs' => $this->parse_attrs(...),
            'headline' => fn ($s) => str($s)->headline(),
        ]);
    }
}
