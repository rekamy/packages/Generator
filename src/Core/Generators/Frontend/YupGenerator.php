<?php

namespace Rekamy\Generator\Core\Generators\Frontend;

use Illuminate\Support\Facades\DB;
use Rekamy\Generator\Core\Generators\BaseGenerator;
use Rekamy\Generator\Core\YamlConfig;

class YupGenerator extends BaseGenerator
{
  use YamlConfig;

  public function generate()
  {
    $tables = DB::getDoctrineSchemaManager()->listTables();

    foreach ($tables as $key => $table) {
      $wkdir = config('rekamygenerator.setup.frontend.path.root') . 'src/models/';
      if (!file_exists($wkdir)) mkdir($wkdir, 0644, true);
      $this->context->newline()->comment($wkdir);
      $model = str($table->getName())->singular();
      $template = $this->handleTemplate(view('generator-templates::frontend.yup-model'), $model->kebab(), $table->getColumns());
      $model = $model->replace('_', '-');
      file_put_contents("{$wkdir}{$model}.ts", $template);
      $this->context->newline()->line('    |_ ' . $model . '.ts');
    }
  }

  public function parse_attrs(mixed $name, mixed $attrs)
  {
    // $yup = ["string", "object", "boolean", "number", "isNotRequired"];
    $name = str($name);
    $rule = str('');

    switch (true) {
      case $name->startsWith('is_'):
        $rule = $rule->append('boolean().default(false)');
        break;
      case $name->endsWith('_id'):
        $rule = $rule->append('string()');
        break;
      case $name->contains(['_at', '_by']):
        $rule = $rule->append('string()');
        break;
      default:
        $rule = $rule->append('string()');
        break;
    }

    $rule = $attrs->getNotNull() ? $rule->append('.required()') : $rule->append('.notRequired()');
    $rule = $rule->append(".label(\"{$name->headline()}\")");

    return $rule->toHtmlString();
  }

  public function handleProps(string $flow, mixed $props = [])
  {
    return collect([
      'model'       => str($flow)->studly(),
      'columns'     => $props,
      'parse_attrs' => $this->parse_attrs(...),
    ]);
  }
}
