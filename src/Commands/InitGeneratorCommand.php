<?php

namespace Rekamy\Generator\Commands;

use Illuminate\Console\Command;
use Rekamy\Generator\Core\BuildConfig;

class InitGeneratorCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate boilerplate for the whole project';

    public $generator;
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->generator = new BuildConfig($this);
        $this->generator->loadConfig();
        $commands = collect();
        $frontendPath = $this->generator->config->setup->frontend->path->root;

        if ($this->confirm('Generate migration from current database?', false)) {
            $commands->push(['artisan' => 'generate:migration']);
        } else {
            $hasLaravelPermission = \Composer\InstalledVersions::isInstalled('spatie/laravel-permission');
            if ($hasLaravelPermission && $this->confirm('Publish spatie/laravel-permission?', false)) {
                $commands->push(['shell' => 'php artisan vendor:publish --provider="Spatie\Permission\PermissionServiceProvider"']);
            }

            if ($this->confirm('Run fresh migration?', true)) {
                $commands->push(['shell' => 'php artisan migrate:fresh --seed']);
            }
        }

        if ($this->confirm('Generate backend from current database?', true)) {
            $commands->push(['artisan' => 'generate:backend']);

            $hasLaravelModules = \Composer\InstalledVersions::isInstalled('nwidart/laravel-modules');
            if (!$hasLaravelModules) {
                $commands->push(['shell' => 'composer require nwidart/laravel-modules']);
            }
        }

        if ($this->confirm('Generate frontend from current database?', true)) {
            $commands->push(['artisan' => 'generate:frontend']);
            if ($this->confirm('Build application on complete generation?', false))
                $commands->push(['shell' => "cd {$frontendPath} && npm run build"]);
        }

        // 
        // if ($this->confirm('Generate workflow?', true)) {
        //     $commands->push(['artisan' => 'generate:workflow']);
        //     if ($this->confirm('Build application on complete generation?', false))
        //         $commands->push(['shell' => "cd {$frontendPath} && npm run build"]);
        // }

        $commandScript = $commands->map(fn ($command) => !empty($command['artisan']) ? 'php artisan ' . $command['artisan'] : $command['shell'])
            ->join(' && ');

        shell_exec($commandScript);
    }
}
