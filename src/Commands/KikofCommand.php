<?php

namespace Rekamy\Generator\Commands;

use Illuminate\Console\Command;
use Rekamy\Generator\Core\BuildConfig;

class KikofCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kikof';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Kick off!';

    public $generator;

    public $commands;

    public function __construct()
    {
        parent::__construct();
        $this->commands = collect();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->getRequiredPackages();
        $this->initializePackages();
        if (!empty($this->commands->join(' && '))) {
            shell_exec($this->commands->join(' && '));
        }
    }

    private function getRequiredPackages()
    {
        // TODO: install composer semver to get specific version
        $devPackages = [
            'composer/semver',
        ];



        $packages = [
            'spatie/laravel-permission',
            'nwidart/laravel-modules',
            'rekamy/laravel-core',
        ];

        $packageToInstalls = collect();

        foreach ($packages as $package) {
            $packageInstalled = \Composer\InstalledVersions::isInstalled($package);
            if (!$packageInstalled) $packageToInstalls->push($package);
        }

        if ($packageToInstalls->count()) {
            $packageToInstalls = $packageToInstalls->unique()->join(' ');
            $this->commands->push("composer require $packageToInstalls");
        }

        $composer = json_decode(file_get_contents('composer.json'), true);
        if (empty($composer['autoload']['psr-4']['Modules\\'])) {
            $composer['autoload']['psr-4']['Modules\\'] = 'Modules/';
            $this->commands->push("npm instal prettier");
            file_put_contents('composer.json', str(json_encode($composer))->replace('\/', '/'));
            $this->commands->push("npx prettier --write composer.json");
            $this->commands->push("composer du");
        }
    }

    private function initializePackages()
    {
        // if ($this->confirm('Publish spatie/laravel-permission?', false)) {
        //     $this->commands->push(['shell' => 'php artisan vendor:publish --provider="Spatie\Permission\PermissionServiceProvider"']);
        // }

        if (!is_dir(base_path('Modules/Crud'))) {
            $this->commands->push('php artisan module:make Crud --plain');
        }

        // if(!is_dir(base_path('resources/client'))) {
        //     $this->commands->push('cd resources && npm init vue@latest client');
        // }
    }
}
