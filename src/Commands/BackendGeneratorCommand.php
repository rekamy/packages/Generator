<?php

namespace Rekamy\Generator\Commands;

use Illuminate\Console\Command;
use Rekamy\Generator\Core\BuildConfig;
use Rekamy\Generator\Core\Generators\Backend\{
    APIDocGenerator,
    APIDocInfoGenerator,
    AuthControllerGenerator,
    BaseControllerGenerator,
    ModelGenerator,
    BlocGenerator,
    RepositoryGenerator,
    ControllerGenerator,
    RequestGenerator,
    CrudableTraitGenerator,
    CrudableRepositoryTraitGenerator,
    HasRepositoryGenerator,
    HasRequestGenerator,
    CrudBlocGenerator,
    CrudBlocInterfaceGenerator,
    CrudControllerGenerator,
    CrudRepositoryInterfaceGenerator,
    CrudRequestInterfaceGenerator,
    ExceptionValidationGenerator,
    APIRoutesGenerator,
    DatatableCriteriaContractsGenerator,
    LengthAwarePaginatorContractsGenerator,
    ServiceProvidersGenerator,
    RequestExtensionContractsGenerator,
    BaseRepositoryContractsGenerator,
    RequestCriteriaContractsGenerator,
    FileUploadContractsGenerator,
    HasAuditorRelationGenerator,
    DependenciesSetupGenerator,
};


class BackendGeneratorCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:backend';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate CRUD API';

    public $generator;
    public $progressbar;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->generator = new BuildConfig($this);
        $this->generator->loadConfig();

        $hasModules = \Composer\InstalledVersions::isInstalled('nwidart/laravel-modules');
        if (!$hasModules) {
            shell_exec("composer install nwidart/laravel-modules");
            $this->call('module:make Crud');
            $this->info('Please setup Modules PSR-4 autoload in composer.json file.');
        } else {
            $this->call('module:make', ['name' => ['Crud']]);
        }

        $this->generator->generateFiles($this->generator->config->generators->backend);

        $hasSwagger = \Composer\InstalledVersions::isInstalled('darkaonline/l5-swagger');
        if ($hasSwagger)
            $this->call('l5-swagger:generate');
    }
}
