<?php

namespace Rekamy\Generator\Commands;

use Illuminate\Console\Command;
use Rekamy\Generator\Core\{BuildConfig, YamlConfig};
// use Symfony\Component\Process\Process;

class ModuleGeneratorCommand extends Command
{
    use YamlConfig;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:module';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate CRUD Module API and UI';

    public $context;
    public $progressbar;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!file_exists(base_path('config/modules.php'))) {
            $this->call('vendor:publish', ['--tag' => ['rekamy-modules']]);
        }
        
        // not using artisan call for it didnt use expected stubs from rekamy/generator
        shell_exec("php artisan module:make Crud");
        
        $this->call('generate:backend');
        $this->call('generate:frontend');
        $this->call('l5-swagger:generate');
    }
}
