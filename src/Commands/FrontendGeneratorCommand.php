<?php

namespace Rekamy\Generator\Commands;

use Illuminate\Console\Command;
use Rekamy\Generator\Core\BuildConfig;
use Rekamy\Generator\Core\Generators\Frontend\{
    BaseTSGenerator,
    BaseVueGenerator,
    VueRouteGenerator,
    CrudBaseTSGenerator,
    CrudBaseVueGenerator,
    CrudFormComponentVueGenerator,
    CrudManageVueGenerator,
    CrudIndexTSGenerator,
    CrudIndexVueGenerator,
    CrudViewTSGenerator,
    CrudViewVueGenerator,
    CrudCreateTSGenerator,
    CrudCreateVueGenerator,
    CrudEditTSGenerator,
    CrudEditVueGenerator,
    DashboardGenerator,
    FrontendModuleGenerator,
    WebRoutesRelayGenerator,
};


class FrontendGeneratorCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:frontend';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Frontend CRUD API';

    public $generator;
    public $progressbar;
    private $defaultIndex = 0;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // dd(config('rekamygenerator.setup.frontend.path.root'));
        $this->generator = new BuildConfig($this);
        $this->generator->loadConfig();
        // $this->template = $this->choice('Choose your template ?', ['Argon', 'Sparker', 'Robust'], $this->defaultIndex);

        $path = config('rekamygenerator.setup.frontend.path.root');

        // if (!file_exists($path)) $this->initFrontend();

        $this->generator->generateFiles(config('rekamygenerator.generators.frontend'));
        if (file_exists($path)) {
            $this->info('Formating files ...');
            shell_exec("cd {$path} && npx prettier --write src/modules/crud/");
        }
        // shell_exec("cd {$path} && npx prettier --write **/*.vue && npm run lint");
    }

    public function initFrontend()
    {
        $path = $this->generator->config->setup->frontend->path->root;
        $source = $this->generator->config->setup->frontend->source;

        shell_exec("git clone --depth=1 {$source} {$path} && rm -rf {$path}/.git");
        shell_exec("cd {$path} && npm i");
    }
}
