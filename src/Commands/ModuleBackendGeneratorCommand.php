<?php

namespace Rekamy\Generator\Commands;

use Illuminate\Console\Command;
use Rekamy\Generator\Core\BuildConfig;
use Nwidart\Modules\Facades\Module;


class ModuleBackendGeneratorCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:module-backend {module} {--P|prefix=-}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Backend Module API';

    public $generator;
    public $progressbar;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // dd($this->argument(), $this->option('prefix'));
        if(!Module::findOrFail($this->argument('module'))) {
            return $this->info("'" . $this->argument('module') . "'" .' Module not found');
        }
        $this->generator = new BuildConfig($this);
        $this->generator->loadConfig();
        $this->generator->setModule($this->argument('module'));
        if($this->option('prefix') != "-"){
            $this->generator->setTablePrefix($this->option('prefix'));
        } else {
            return $this->info('Please provide prefix');
        }

        $hasModules = \Composer\InstalledVersions::isInstalled('nwidart/laravel-modules');
        if (!$hasModules) {
            return $this->info('Please install nwidart/laravel-modules');
        }

        $this->generator->generateFiles($this->generator->config->generators->backend);

        $hasSwagger = \Composer\InstalledVersions::isInstalled('darkaonline/l5-swagger');
        if ($hasSwagger)
            $this->call('l5-swagger:generate');
    }
}
