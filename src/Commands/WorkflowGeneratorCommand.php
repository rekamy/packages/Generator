<?php

namespace Rekamy\Generator\Commands;

use Illuminate\Console\Command;
use Rekamy\Generator\Core\YamlConfig;
use Rekamy\Generator\Core\Generators\Workflow\{
    BlocGenerator,
    ComponentGenerator,
    ControllerGenerator,
    ModelGenerator,
    RequestGenerator,
    RouteGenerator,
};

class WorkflowGeneratorCommand extends Command
{
    use YamlConfig;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:workflow
                            {file=workflow : Generate workflow defined from specified file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Workflow from workflow.yaml';

    public $workflow;
    public $module;
    public $progressbar;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->newLine(3)->alert('Experimental Feature (Workflow)');

        if (!$this->handleCheck()) return;

        $this->handleYaml();

        $this->generateFiles(config('rekamygenerator.generators.workflow'));

        $this->handleDone();
    }

    public function handleCheck()
    {
        $specified = "workflow/{$this->argument('file')}.yaml";

        if (!sizeof(glob("workflow/*.yaml")) && !file_exists($specified)) {

            $this->newLine()->warn($specified . ' file not found');

            if ($this->confirm('create one?', true)) {
                try {
                    $this->newLine()->line('creating workflow/ directory..');
                    if (!file_exists('workflow/'))
                        mkdir('workflow');
                } catch (\Throwable $th) {
                    $this->newLine()->warn($th);
                }
                $this->newLine()->line("creating {$specified} file..");
                $this->writeYaml($specified, $this->getStub());
                $this->handleDone('File created', 1);
            } else {
                $this->newLine()->alert('Skipped file creation');
                return false;
            };
        }

        return true;
    }

    public function getStub()
    {
        return $this->readYaml(__DIR__ . '/../../schema.yaml');
    }

    public function handleYaml()
    {
        $getModule = fn ($file) => str($file)->beforeLast('.yaml')->toString();

        $this->workflow = collect(glob("workflow/*.yaml"))
            ->each(fn ($file) => !file_exists($getModule($file)) && mkdir($getModule($file), 0644, true))
            ->map(fn ($file) => [$getModule($file) => $this->readYaml($file)['workflows']])
            ->collapse();
    }

    public function handleDone(string $message = 'Process Completed!', int $count = 0)
    {
        $this->newline($count)->info($message);
    }
}
