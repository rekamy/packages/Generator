<?php

namespace Rekamy\Generator;

use Illuminate\Support\ServiceProvider;
use Rekamy\Generator\Commands\{
    FrontendGeneratorCommand,
    KikofCommand,
    InitGeneratorCommand,
    BackendGeneratorCommand,
    MigrationGeneratorCommand,
    WorkflowGeneratorCommand,
    ModuleGeneratorCommand,
    ModuleBackendGeneratorCommand
};

class GeneratorServiceProvider extends ServiceProvider
{
    protected $commands = [
        KikofCommand::class,
        InitGeneratorCommand::class,
        BackendGeneratorCommand::class,
        FrontendGeneratorCommand::class,
        MigrationGeneratorCommand::class,
        WorkflowGeneratorCommand::class,
        ModuleGeneratorCommand::class,
        ModuleBackendGeneratorCommand::class
    ];

    public function boot()
    {
        // publishables
        $this->publishes([            
            dirname(__DIR__) . '/config/rekamygenerator.php' => config_path('rekamygenerator.php'),
        ], 'rekamy-generator');

        $this->publishes([            
            dirname(__DIR__) . '/config/rekamymodules.php' => config_path('modules.php'),
        ], 'rekamy-modules');

        $this->publishes([            
            dirname(__DIR__) . '/src/Commands/stubs/nwidart-stubs' => base_path('stubs/nwidart-stubs'),
        ], 'rekamy-stubs');

        $this->loadViewsFrom(__DIR__ . '/templates', 'generator-templates');
    }

    public function register()
    {
        $this->mergeConfigFrom(dirname(__DIR__) . '/config/rekamygenerator.php', 'rekamygenerator');
        $this->mergeConfigFrom(dirname(__DIR__) . '/config/rekamymodules.php', 'modules');

        if ($this->app->runningInConsole()) {
            $this->commands($this->commands);
        }
    }
}
